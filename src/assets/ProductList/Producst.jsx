let getRandom = () => {
  for (let i = 0; i < 100; i++) {
    let rand = Math.floor(Math.random() * 500);
    let url = `https://picsum.photos/id/${rand}/200/300`;
    return url;
  }
};

export const PRODUCT_LIST = [
  {
    id: 1,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 2,
    productName: "Phân bón D.A.P (25Kg)",
    price: 800000,
    hinhANh: getRandom(),
  },
  {
    id: 3,
    productName: "Phân bón Hữu Cơ (50Kg)",
    price: 9500000,
    hinhANh: getRandom(),
  },
  {
    id: 5,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 6,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 7,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 8,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 9,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 10,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
  {
    id: 11,
    productName: "Phân bón N.K.P (50Kg)",
    price: 1000000,
    hinhANh: getRandom(),
  },
];

import { Navigate, Route, Routes } from "react-router";
import { BrowserRouter } from "react-router-dom";

import "./App.css";
import MainLayout from "./Layout/MainLayout";
import Products from "./Pages/Products/Products";
import Home from "./Pages/Home/Home";
import Contact from "./Pages/ContactUs/Contact";
import Intro from "./Pages/Intro/Intro";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<Navigate to={"/home"} />} />
        <Route path="/" element={<MainLayout Content={<Home />} />} />
        <Route path="/home" element={<MainLayout Content={<Home />} />} />

        <Route
          path="/products"
          element={<MainLayout Content={<Products />} />}
        />
        <Route path="/contact" element={<MainLayout Content={<Contact />} />} />
        <Route path="/intro" element={<MainLayout Content={<Intro />} />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;

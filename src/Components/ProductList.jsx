import React, { useEffect, useState } from "react";
import { Card } from "antd";
import { PRODUCT_LIST } from "../assets/ProductList/Producst";

const { Meta } = Card;

export default function ProductList() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    setProducts(PRODUCT_LIST);
  }, [products]);

  const renderProducts = () => {
    return products.map((product) => {
      return (
        <Card
          key={product.id}
          hoverable
          style={{
            width: 230,
            margin: "0 15px 15px 15px",
          }}
          cover={
            <img
              style={{ height: 200, padding: "10px" }}
              alt="example"
              src={product.hinhANh}
            />
          }
        >
          <Meta
            title={product.productName}
            description={
              <p className="bg-red-600 text-white font-bold inline-block p-1 rounded">
                Giá: {product.price}
              </p>
            }
          />
        </Card>
      );
    });
  };

  return <div className="grid grid-cols-4">{renderProducts()}</div>;
}

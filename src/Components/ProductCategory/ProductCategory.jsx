import React, { useEffect } from "react";
import { CaretRightOutlined } from "@ant-design/icons";
import "./ProductCategory.css";

export default function ProductCategory() {
  const onClickHandle = () => {};

  useEffect(() => {
    let liEl = document.querySelectorAll("ul#categoryList>li");
    liEl.forEach((liElement) => {
      liElement.addEventListener("click", () => {
        document.querySelector(".active")?.classList.remove("active");
        liElement.classList.add("active");
      });
    });
  }, []);

  return (
    <div className="category">
      <ul id="categoryList">
        <li onClick={onClickHandle}>
          <p>
            <CaretRightOutlined /> PHÂN BÓN
          </p>{" "}
          <span>(0)</span>
        </li>
        <li>
          <p>
            <CaretRightOutlined /> THUỐC TRỪ SÂU
          </p>{" "}
          <span>(0)</span>
        </li>
        <li>
          <p>
            <CaretRightOutlined /> THUỐC DIỆT CỎ
          </p>{" "}
          <span>(0)</span>
        </li>
        <li>
          <p>
            <CaretRightOutlined /> THUỐC DIỆT CHUỘT
          </p>{" "}
          <span>(0)</span>
        </li>
        <li>
          <p>
            <CaretRightOutlined /> THUỐC TĂNG TRƯỞNG
          </p>{" "}
          <span>(0)</span>
        </li>
      </ul>

      {/* <CaretRightOutlined />
      <input type="radio" className="radio--fertilizer" />
      <label for="radio--fertilizer"></label>
      <CaretRightOutlined />
      <input type="radio" className="radio--" /> */}
      
    </div>
  );
}

import React, { useEffect, useState } from "react";
import { Button, Layout, Menu } from "antd";
import {
  DropboxOutlined,
  HomeOutlined,
  IdcardOutlined,
  PhoneOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";
import { NavLink } from "react-router-dom";
const { Header } = Layout;

const items = [
  {
    label: (
      <NavLink to="/">
        <span className="font-bold">TRANG CHỦ</span>
      </NavLink>
    ),
    key: "home",
    icon: <HomeOutlined />,
  },
  {
    label: (
      <NavLink to="/intro">
        <span className="font-bold">GIỚI THIỆU</span>
      </NavLink>
    ),
    key: "intro",
    icon: <IdcardOutlined />,
  },
  {
    label: (
      <NavLink to="/products">
        <span className="font-bold">SẢN PHẨM</span>
      </NavLink>
    ),
    key: "product",
    icon: <DropboxOutlined />,
  },

  {
    label: (
      <NavLink to="/contact">
        <span className="font-bold">LIÊN HỆ</span>
      </NavLink>
    ),
    key: "contact",
    icon: <PhoneOutlined />,
  },
];
const items2 = [
  {
    label: <UnorderedListOutlined />,
    key: "SubMenu",
    children: [
      {
        label: (
          <NavLink to="/">
            <span className="font-bold">TRANG CHỦ</span>
          </NavLink>
        ),
      },
      {
        label: (
          <NavLink to="/intro">
            <span className="font-bold">GIỚI THIỆU</span>
          </NavLink>
        ),
      },
      {
        label: (
          <NavLink to="/products">
            <span className="font-bold">SẢN PHẨM</span>
          </NavLink>
        ),
      },
      {
        label: (
          <NavLink to="/contact">
            <span className="font-bold">LIÊN HỆ</span>
          </NavLink>
        ),
      },
    ],
  },
];
export default function HeaderComponent() {
  const [current, setCurrent] = useState("home");
  const [width, setWidth] = React.useState(window.innerWidth);
  const bP = 620;

  useEffect(() => {
    const handleResize = () => setWidth(window.innerWidth);
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const onClick = (e) => {
    setCurrent(e.key);
  };

  return (
    <>
      {width < bP ? (
        <Menu
          items={items2}
          mode="horizontal"
          className="sticky top-0 left-[-10px] w-full"
          theme="dark"
        />
      ) : (
        <>
          <Header className="sticky top-0 w-full flex flex-col bg-[#fff] h-[250px] first-letter">
            <div className="flex items-start justify-between h-full w-full">
              <div className="w-[200px] h-[200px] flex items-center pt-10 invisible">
                <img className="w-full h-full" src="" alt="Logo photo" />
              </div>
              <div className="flex flex-col items-center justify-end pt-10">
                <h1 className=" text-black text-xs md:text-3xl lg:text-5xl">
                  CỬA HÀNG VẬT TƯ NÔNG NGHIỆP TÁM THỌ
                </h1>
                <p className="text-xs text-black md:text-xl lg:text-1xl">
                  Chuyên kinh doanh phân bón, thuốc bảo vệ thực vật, hạt giống,
                  vật tư nông nghiệp
                </p>
              </div>
              <Button
                className="flex items-center bg-red-600 w-[100px] h-[70px] rounded-2xl mt-10 md:w-[200px] md:h-[70px] "
                type="primary"
              >
                <PhoneOutlined style={{ fontSize: "30px" }} />{" "}
                <p className="text-xs font-bold text-white md:text-xl">
                  0909 649 532
                </p>
              </Button>
            </div>
            <Menu
              className="absolute bottom-0 left-[-10px] w-full"
              theme="dark"
              mode="horizontal"
              items={items}
              selectedKeys={[current]}
              onClick={onClick}
            />
          </Header>
        </>
      )}
    </>
  );
}

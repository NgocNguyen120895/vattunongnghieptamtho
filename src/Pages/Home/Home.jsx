import React, { Fragment } from "react";
import bgPic from "../../assets/background/vtnntamtho.png";
import Style from "./homeStyle.module.css";
import { MenuOutlined } from "@ant-design/icons";
import ProductList from "../../Components/ProductList";
import ProductCategory from "../../Components/ProductCategory/ProductCategory";

export default function Home() {
  return (
    <>
      <div>
        <img src={bgPic} alt="mybackground" />
      </div>

      <div className="grid grid-cols-4">
        <div className={Style.cover}>
          <div className={Style.wraper}>
            <div className={Style.title}>
              <MenuOutlined /> <span>DANH MỤC SẢN PHẨM</span>
            </div>
          </div>
          <ProductCategory />
        </div>
        <div className="col-span-3 m-4">
          <ProductList />
        </div>
      </div>
    </>
  );
}

import React, { Fragment } from "react";
import Style from "./Contact.module.css";

const key = "yourKey";

export default function Contact() {
  return (
    <div className="outer grid grid-cols-2">
      <div className={Style.address}>
        <h2>CỬA HÀNG VẬT TƯ NÔNG NGHIỆP TÁM THỌ</h2>
        <div></div>
        <p>
          Số điện thoại: 0909 649 522 (Anh Thọ),
          <br /> hoặc số điện thoại: 0399 222 426 (Chị Thùy).
        </p>
        <p>
          Địa chỉ: Số 71, Ấp Thạnh Lợi A2, Xã Tân Long, Huyện Phụng Hiệp, Tỉnh
          Hậu Giang
        </p>
      </div>
      <div className={Style.map}>
        <h2>GOOGLE MAP</h2>
        <div></div>
      </div>
    </div>
  );
}

import React from "react";
import Header from "../Components/Header";
import Footer from "../Components/Footer";

export default function MainLayout({ Content }) {
  return (
    <>
      <Header />
      {Content}
      <Footer />
    </>
  );
}
